$(function () {
    $("[data-toggle='tooltip']").tooltip();
});
$(function () {
    $("[data-toggle='popover']").popover();
});
$('.carousel').carousel({
    interval: 3000
});
$('#contacto').on('shown.bs.modal', function (e) {
    console.log('el modal se está mostrando!');
    $('.btn-contacto').each(function (btn) {
        $(this).prop("disabled", true);
        $(this).removeClass("btn-outline-success");
        $(this).addClass("btn-default");
    });

});
$('#contacto').on('show.bs.modal', function (e) {
    console.log('el modal se va a mostrar!');
});
$('#contacto').on('hide.bs.modal', function (e) {
    console.log('el modal se va a cerrar!');
    $('.btn-contacto').each(function (btn) {
        $(this).prop("disabled", false);
        $(this).addClass("btn-outline-success");
        $(this).removeClass("btn-default");
    });
});
$('#contacto').on('hidden.bs.modal', function (e) {
    console.log('el modal cerró!');
});